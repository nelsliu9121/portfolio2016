import angular from 'angular';
import 'jquery';
import 'bootstrap-loader';

import routesConfig from './routes';

import './index.scss';
import {framework} from './framework';

import {appWorks} from './app/works/works';

import {appSidebar} from './app/layout/appSidebar';
import {appFooter} from './app/layout/appFooter';
import {main} from './app/main';

export const app = 'app';

angular
	.module(app, [framework, appWorks])
	.config(routesConfig)
	.component("main", main)
	.component("appFooter", appFooter)
	.component("appSidebar", appSidebar);
