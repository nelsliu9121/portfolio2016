import angular from 'angular';

import enUSLang from './enUS.json';
// import {zhTWLang} from './zhTW.json';

export const i18n = "app.i18n";

angular
	.module(i18n, ['pascalprecht.translate'])
	.config(function ($translateProvider) {
		$translateProvider
			.translations('en-us', enUSLang)
			.translations('zh-tw', zhTwLang)
			.preferredLanguage('zh-tw');
	});
