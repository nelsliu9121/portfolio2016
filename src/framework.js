import 'angular-ui-router';
import 'angular-strap';
import 'angular-moment';
import 'angular-breadcrumb';
import 'angular-fontawesome';
import 'angular-translate';

import {i18n} from './languages/index';

export const framework = 'framework';

angular.module(framework, [
	'ui.router',
	'mgcrea.ngStrap',
	'angularMoment',
	'ncy-angular-breadcrumb',
	'picardy.fontawesome',
	i18n
]);
