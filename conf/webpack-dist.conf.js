const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const SplitByPathPlugin = require('webpack-split-by-path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');

module.exports = {
	module: {
		preLoaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'eslint'
			}
		],

		loaders: [
			{
				test: /.json$/,
				loaders: ['raw']
			}, {
				test: /\.(css|scss)$/,
				loaders: ExtractTextPlugin.extract('style', 'css?minimize!sass', 'postcss')
			}, {
				test: /\.coffee$/,
				exclude: /node_modules/,
				loader: 'ng-annotate-loader!coffee-loader'
			}, {
				test: /\.js/,
				exclude: /node_modules/,
				loader: 'ng-annotate-loader!babel-loader'
			}, {
				test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
				loader: 'imports?jQuery=jquery'
			}, {
				test: /\.(woff2?|svg)$/,
				loader: 'url?limit=10000'
			}, {
				test: /\.(ttf|eot)$/,
				loader: 'file'
			}, {
				test: /\.(pug|jade)$/,
				loader: "pug"
			}
		]
	},
	plugins: [
		new webpack
			.optimize
			.OccurrenceOrderPlugin(),
		new webpack.NoErrorsPlugin(),
		new HtmlWebpackPlugin({
			template: conf
				.path
				.src('index.pug'),
			inject: true
		}),
		new webpack
			.optimize
			.UglifyJsPlugin({
				compress: {
					unused: true,
					dead_code: true
				} // eslint-disable-line camelcase
			}),
		new SplitByPathPlugin([
			{
				name: 'vendor',
				path: path.join(__dirname, '../node_modules')
			}
		]),
		new ExtractTextPlugin('/index-[contenthash].css')
	],
	postcss: () => [autoprefixer],
	output: {
		path: path.join(process.cwd(), conf.paths.dist),
		filename: '[name]-[hash].js'
	},
	resolve: {
		extensions: ['', '.json', ".js", ".coffee"]
	},
	entry: {
		app: [
			`./${conf
				.path
				.src('index')}`,
			`./${conf
				.path
				.tmp('templateCacheHtml.js')}`
		]
	},
	sassResources: './sass-resources.scss'
};
