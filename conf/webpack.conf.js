const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
	module: {
		loaders: [
			{
				test: /.json$/,
				loaders: ['raw']
			}, {
				test: /\.(css|scss)$/,
				loaders: ['style', 'css', 'sass', 'postcss']
			}, {
				test: /\.coffee$/,
				exclude: /node_modules/,
				loader: 'ng-annotate-loader!coffee-loader'
			}, {
				test: /\.js/,
				exclude: /node_modules/,
				loader: 'ng-annotate-loader!babel-loader'
			}, {
				test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
				loader: 'imports?jQuery=jquery'
			}, {
				test: /\.(woff2?|svg)$/,
				loader: 'url?limit=10000'
			}, {
				test: /\.(ttf|eot)$/,
				loader: 'file'
			}, {
				test: /\.(jpg|jpeg|png)$/,
				loader: 'url?limit=10000'
			}, {
				test: /\.(pug|jade)$/,
				loader: "pug-html-loader"
			}
		]
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoErrorsPlugin(),
		new HtmlWebpackPlugin({
			template: conf.path.src('index.pug'),
			inject: true
		})
	],
	postcss: () => [autoprefixer],
	debug: true,
	devtool: 'cheap-module-eval-source-map',
	output: {
		path: path.join(process.cwd(), conf.paths.tmp),
		filename: 'index.js'
	},
	resolve: {
		extensions: ['', '.json', ".js", ".coffee"]
	},
	entry: `./${conf.path.src('index')}`,
	sassResources: './sass-resources.scss'
};
