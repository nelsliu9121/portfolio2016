module.exports = {
	module: {
		preLoaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'eslint'
			}
		],

		loaders: [
			{
				test: /.json$/,
				loaders: [
					'json'
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loaders: [
					'ng-annotate',
					'babel'
				]
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|.*\.spec\.js)/,
				loader: 'isparta'
			},
			{ test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports?jQuery=jquery' },
			{ test: /\.(woff2?|svg)$/, loader: 'url?limit=10000' },
{ test: /\.(ttf|eot)$/, loader: 'file' },
		]
	},
	plugins: [],
	debug: true,
	devtool: 'cheap-module-eval-source-map'
};
