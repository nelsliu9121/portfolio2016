const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const angularTemplatecache = require('gulp-angular-templatecache');
const pug = require("gulp-pug");
const insert = require('gulp-insert');

const conf = require('../conf/gulp.conf');

gulp.task('partials', partials);

function partials() {
	return gulp.src(conf.path.src('app/**/*.pug'))
		.pipe(pug())
		.pipe(htmlmin())
		.pipe(angularTemplatecache('templateCacheHtml.js', {
			module: conf.ngModule,
			root: 'app'
		}))
		.pipe(insert.prepend(`import angular from 'angular';`))
		.pipe(gulp.dest(conf.path.tmp()));
}
